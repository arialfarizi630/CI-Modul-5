<?php
namespace PHPUnit\Framework;
use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{
	public function testAdd($a, $b, $expected)
	{
		$this->assertSame($expected,$a+$b);
	}
	public function additionProvider()
	{
		return [
			[0, 0, 0],
			[0, 1, 1],
			[1, 0, 1],
			[1, 1, 3]
		];
	}
}
?>